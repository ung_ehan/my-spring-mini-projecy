package com.example.demo.service.impl;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getBookList() {
        return this.bookRepository.getBookList();
    }

    @Override
    public Book findOne(int id) {
        return this.bookRepository.findOne(id);
    }

    @Override
    public boolean update(Book book) {
        return this.bookRepository.update(book);
    }

    @Override
    public boolean delete(int id) {
        return this.bookRepository.delete(id);
    }

    @Override
    public boolean create(Book book) {
        return this.bookRepository.create(book);
    }
}
