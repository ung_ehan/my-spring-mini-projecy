package com.example.demo.repositories;

import com.example.demo.models.Category;
import com.example.demo.service.CategoryService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("SELECT * FROM tb_category")
    List<Category> getAllCategory();

}
