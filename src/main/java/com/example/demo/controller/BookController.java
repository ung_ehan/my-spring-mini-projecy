package com.example.demo.controller;

import com.example.demo.models.Book;
import com.example.demo.models.Category;
import com.example.demo.service.BookService;
import com.example.demo.service.CategoryService;
import com.example.demo.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BookController {

    private BookService bookService;

    private CategoryService categoryService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    public BookController(BookService bookService, CategoryService categoryService){
        this.bookService = bookService;
        this.categoryService = categoryService;
    }

    @GetMapping({"/index", "", "/home"})
    public String index(ModelMap modelMap){
        List<Book> bookList = this.bookService.getBookList();
        modelMap.addAttribute("books",bookList);
        return "book/index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable int id, ModelMap modelMap){
        System.out.println(id);
        Book book = this.bookService.findOne(id);
        modelMap.addAttribute("book",book);
        return "book/view-detail";
    }

    @GetMapping("update/{book_id}")
    public String showUpdateForm(@PathVariable int book_id, ModelMap modelMap){
        Book book = this.bookService.findOne(book_id);
        modelMap.addAttribute("isNew",false);
        modelMap.addAttribute("book",book);
        return "book/create";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute("book") Book book, MultipartFile file){

        String filename = this.uploadService.upload(file,null);

        if (!file.isEmpty()){
            book.setThumbnail("/image-btb/"+filename);
        }

        System.out.println(book);
        this.bookService.update(book);
        return "redirect:/index";
    }

    @GetMapping("delete/{id}")
    public String deleteBook(@PathVariable("id") int id){
        this.bookService.delete(id);
        return "redirect:/index";
    }

    @GetMapping("/create")
    public String createForm(Model model){
        List<Category> categories = this.categoryService.getAllCategory();
        model.addAttribute("isNew",true);
        model.addAttribute("categories",categories);
        model.addAttribute("book",new Book());
        return "book/create";
    }

    /** @Valid annotation is used to make all fields that have been validated followed the rule that we have set
     *  BindingResult is used to check whether all fields that we have been validated are valid or not*/
    /** */
    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult,MultipartFile file, Model model){


//        model.addAttribute("filenames",filename);

        /** it is used to check the input field that we have validated, if it has any errors it will return to
         *  book/create page again. */
        if (bindingResult.hasErrors()){
            model.addAttribute("isNew",true);
            return "book/create";
        }

        String filename = this.uploadService.singleFileUpload(file,null);
        book.setThumbnail("/image-btb/"+filename);

        System.out.println(book);
        this.bookService.create(book);
        return "redirect:/index";
    }

    @GetMapping("/upload-multi-file")
    public String uploadMultiFileForm(){
        return "book/upload-multi-file";
    }

    @PostMapping("/upload-multi-file/submit")
    public String uploadMultiFileSubmit(@RequestParam("file") List<MultipartFile> files){
        List<String> filenames = this.uploadService.upload(files,"test/");
        return "";
    }
}
