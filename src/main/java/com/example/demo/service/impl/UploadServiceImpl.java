package com.example.demo.service.impl;

import com.example.demo.service.UploadService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UploadServiceImpl implements UploadService {
    @Value("${file.client.path}")
    private String CLIENT_PATH;
    @Value("${file.server.path}")
    private String SERVER_PATH;

    @Override
    public String singleFileUpload(MultipartFile file, String folder) {

        if(file == null){
            return null;
        }

        if (folder == null){
            folder = "";
        }

        /** The statements below is used to create new path directory name [btb] in our local machine and
         *  check whether it has this kind of directory or not yet, if it didn't have it will create a new one.*/
        File path = new File(SERVER_PATH+folder);
        if (!path.exists()){
            path.mkdir();
        }

        /** it is used to get file's original name*/
        String filename = file.getOriginalFilename();

        /** it is used to get file's extension*/
        String extension = filename.substring(filename.lastIndexOf(".")+1);

        System.out.println(filename);
        System.out.println(extension);

        /** it is used to generate new random file name for our file in order to avoid file name redundancy
         *  in our resource directory. */
        filename = UUID.randomUUID() + "." + extension;

        /** It is used to copy file that we have been uploaded into the new directory [btb] */
        try {
            Files.copy(file.getInputStream(), Paths.get(SERVER_PATH+folder, filename));
        }catch(IOException e){
            System.out.println(e);
        }
        return folder+filename;
    }

    @Override
    public List<String> multipleFileUpload(List<MultipartFile> files, String folder) {

        List<String> filenames = new ArrayList<>();
        files.forEach(file->
            filenames.add(this.singleFileUpload(file,folder))
        );
        return filenames;
    }

    @Override
    public String upload(MultipartFile file) {
        return this.singleFileUpload(file,null);
    }

    @Override
    public String upload(MultipartFile file, String folder) {
        return this.singleFileUpload(file, folder);
    }

    @Override
    public List<String> upload(List<MultipartFile> files) {
        return this.multipleFileUpload(files,null);
    }

    @Override
    public List<String> upload(List<MultipartFile> files, String folder) {
        return this.multipleFileUpload(files,folder);
    }
}
