CREATE TABLE tb_category(
  id SERIAL PRIMARY KEY,
  name VARCHAR
);

CREATE TABLE tb_books(
  id SERIAL PRIMARY KEY,
  title VARCHAR ,
  author VARCHAR ,
  publisher VARCHAR ,
  thumbnail VARCHAR,
  cate_id INT REFERENCES tb_category(id)
);
