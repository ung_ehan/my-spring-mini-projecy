package com.example.demo.service;

import com.example.demo.models.Book;

import java.util.List;

public interface BookService {
    List<Book> getBookList();
    Book findOne(int id);
    boolean update(Book book);
    boolean delete(int id);
    boolean create(Book book);
}
