package com.example.demo.repositories.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String selectBookListProvider(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books b");
            INNER_JOIN("tb_category c ON b.cate_id=c.id");
            ORDER_BY("b.id ASC");
        }}.toString();
    }

    public String selectBookProvider(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
}

    public String insertBookProvider(){
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("publisher","#{publisher}");
            VALUES("thumbnail","#{thumbnail}");
        }}.toString();
    }

    public String updateBookProvider(){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{title}, author=#{author}, publisher=#{publisher}, thumbnail=#{thumbnail}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String deleteBookProvider(){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }
}
